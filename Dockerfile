FROM node:16.3.0-alpine 

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

RUN npm install -g ts-node
RUN npm install

# RUN npm run build
# If you are building your code for production
# RUN npm tsc

# Bundle app source
COPY . .

EXPOSE 3000

# CMD [ "node", "src/server.ts" ]
# CMD ["ts-node", "src/server.ts"]
# CMD ["npm", "run dev"]
ENTRYPOINT npm run start
