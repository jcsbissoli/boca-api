# Boca-API

Trabalho de Banco de dados

## Rodar aplicação

Para rodar a aplicação e necessário a utilização do docker e docker-compose!!

### Build

```
 sudo docker build -t boca-api . 
```

Esse comando gera uma image docker com a API.

### Subir containers:

```
sudo docker-compose up -d
```
Obs.: Caso queira ver os logs de execução dos container retire a flex -d

```
sudo docker-compose up
```

## Uso

Apos iniciar a aplicação ela estará rodando na porta 3001 no localhost. Respondendo as rotas com base_url = http://localhost:3001/api

---

### Contest

### Rota
(GET) localhost:3001/api/contest

---

#### Input
```
nul
```


#### Output
```
[
  {
    "contestnumber": 0,
    "contestname": "Fake contest (just for initial purposes)",
    "conteststartdate": 1644841630,
    "contestduration": 0,
    "contestlastmileanswer": 0,
    "contestlastmilescore": 0,
    "contestlocalsite": 1,
    "contestpenalty": 1200,
    "contestmaxfilesize": 100000,
    "contestactive": true,
    "contestmainsite": 1,
    "contestkeys": "",
    "contestunlockkey": "",
    "contestmainsiteurl": "",
    "updatetime": 1644841630
  }
]
```
### Testes:

Os testes foram realizados no insomnia e foi gerado um arquivo testes.json que deve ser importado.

Cada funcionalidade possui seus testes em subpastas, por exemplo a subpasta contest que apresenta as seguintes rotas:

get contests: GET localhost:3001/api/contest

get contests by id: GET localhost:3001/api/contest/0

post: POST localhost:3001/api/contest

put: PUT localhost:3001/api/contest/1

delete: DELETE localhost:3001/api/contest/1 

As funcionalidades de associar linguagens aos problemas são testadas na subpasta languages_by_problem.

Para cadastrar linguagens ao problema, foi utilizada a tabela de referências lang_problems_ref, que permite associar um langnumber a um problenumber.  

É passado na request o langnumber_array com o número das linguagens a serem cadastradas ao problema. Conforme abaixo:

### Rota
(POST) localhost:3001/api/problem/0/language

---

#### Input
```
{
	"langnumber_array": [3,2]
}
```


#### Output
```
"cadastrada(s) linguagem(s)"
```

As funcionalidades relacionadas a workingtable podem ser testadas na subpasta workingtable.

Foi utilizada a tabela de referências working_user_ref, que permite associar um usernumber a um working_id.
