import express from 'express'
import * as dotenv from "dotenv";
import {Request, Response} from 'express'
import router from './router';
import bodyParser from "body-parser"

dotenv.config({ path: __dirname+'/../.env' });

const app = express()

// app.use(express.json)


app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(bodyParser.raw());

app.use('/api', router)

const port = process.env.PORT

app.get('/', (req: Request, res: Response) => {
    return res.json({message: 'Deu bom'})
})


app.listen(port, () => {
    console.log('Servidor rodando na porta ', port)
})