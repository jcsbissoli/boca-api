import * as working_table from './migrations/working_table'
import * as ref_lang_problens from './migrations/lang_problems_ref_table'
import * as working_user_ref from './migrations/working_user_ref'



ref_lang_problens.down()
working_table.down()
working_user_ref.down()