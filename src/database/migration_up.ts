import * as working_table from './migrations/working_table'
import * as ref_lang_problems from './migrations/lang_problems_ref_table'
import * as working_user_ref from './migrations/working_user_ref'
import * as add_working_to_problems from './migrations/add_working_to_problem'



ref_lang_problems.up()
working_table.up()
working_user_ref.up()
add_working_to_problems.up()