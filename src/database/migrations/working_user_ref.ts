import db from "../../database/conection";


const create_query = `
    CREATE TABLE working_user_ref (
        id SERIAL,
        usernumber INT NOT NULL,
        working_id INT NOT NULL,
        PRIMARY KEY (id)    
    );`;


const delete_query = `DROP TABLE working_user_ref;`

const exists_table = `
SELECT EXISTS (
    SELECT FROM
        information_schema.tables
        WHERE
        table_schema LIKE 'public' AND
        table_type LIKE 'BASE TABLE' AND
        table_name = 'working_user_ref');
    `



export const up = async () => {
    try {
        const existes = await db.query(exists_table)
        if (existes && existes.rows[0].exists) {
        console.log('Tabela "working_user_ref" já existe!')
        }
        else {

            await db.query(create_query);
            console.log('Tabela "working_user_ref" criada com sucesso')
        }

    } catch (error) {
        console.error('Erro ao criar tabela "working_user_ref": ', error)
    }
    return
}

export const down = async () => {
    // console.log('asntes => ', saida)
    
    try {
        const exists = await db.query(exists_table)
        if(exists.rows[0].exists){
            await db.query(delete_query);
            console.log('Tabela "working_user_ref" deletada com suceso!')
        }
        else {
            console.log("Tabela working_user_ref não existe")
        }
    } catch (error) {
        console.error("Erro ao deletar tabela working_user_ref: ", error)
    }
    return
}
