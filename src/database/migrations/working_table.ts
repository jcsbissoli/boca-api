import db from "../conection";

const table_name = "workingtable";

const create_query = `
    CREATE TABLE ${table_name} (
        id INT,
        name VARCHAR (45),
        contestnumber INT,
        endDate VARCHAR (15),
        maxfileSize INT,
        updadtedAt VARCHAR (15),
        createdAt VARCHAR (15),
        deletedAt VARCHAR (15),
        isMultilogin BOOLEAN,
        isDeleted BOOLEAN,
        PRIMARY KEY (id)    
    );`;



const delete_query = `DROP TABLE ${table_name};`;

const exists_table = `
SELECT EXISTS (
    SELECT FROM
        information_schema.tables
        WHERE
        table_schema LIKE 'public' AND
        table_type LIKE 'BASE TABLE' AND
        table_name = 'workingtable');
    `;

export const up = async () => {
  try {
    const existes = await db.query(exists_table)
    if (existes && existes.rows[0].exists) {
    console.log('Tabela "workingtable" já existe!')
    }
    else {

        await db.query(create_query);
        console.log('Tabela "workingtable" criada com sucesso')
    }

} catch (error) {
    console.error('Erro ao criar tabela "workingtable": ', error)
}
};

export const down = async () => {
  try {
    const exists = await db.query(exists_table);
    if (exists.rows[0].exists) {
      await db.query(delete_query);
      console.log(`Tabela ${table_name} deletada com suceso!`);
    } else {
      console.log(`Tabela ${table_name} não existe`);
    }
  } catch (error) {
    console.error(`Erro ao deletar tabela ${table_name}: `, error);
  }
  return;
};
