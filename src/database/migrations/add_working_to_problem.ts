import db from "../../database/conection";


const create_query = `
    ALTER TABLE problemtable 
        ADD COLUMN IF NOT EXISTS working_id INT
    ;`;

    
const exists_table = `
SELECT EXISTS (
    SELECT FROM
        information_schema.tables
        WHERE
        table_schema LIKE 'public' AND
        table_type LIKE 'BASE TABLE' AND
        table_name = 'problemtable');
    `



export const up = async () => {
    try {
        const existes = await db.query(exists_table)
        if (existes && existes.rows[0].exists) {
        
        await db.query(create_query);
        console.log('Tabela "problem alterada" já existe!')
        
    }
        

    } catch (error) {
        console.error('Erro ao criar tabela "problems": ', error)
    }
    return
}

