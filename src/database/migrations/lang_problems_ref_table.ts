import db from "../../database/conection";


const create_query = `
    CREATE TABLE lang_problems_ref (
        id SERIAL,
        langnumber INT NOT NULL,
        problemnumber INT NOT NULL,
        PRIMARY KEY (id)    
    );`;


const delete_query = `DROP TABLE lang_problems_ref;`

const exists_table = `
SELECT EXISTS (
    SELECT FROM
        information_schema.tables
        WHERE
        table_schema LIKE 'public' AND
        table_type LIKE 'BASE TABLE' AND
        table_name = 'lang_problems_ref');
    `



export const up = async () => {
    try {
        const existes = await db.query(exists_table)
        if (existes && existes.rows[0].exists) {
        console.log('Tabela "lang_problems_ref" já existe!')
        }
        else {

            await db.query(create_query);
            console.log('Tabela "lang_problems_ref" criada com sucesso')
        }

    } catch (error) {
        console.error('Erro ao criar tabela "lang_problems_ref": ', error)
    }
    return
}

export const down = async () => {
    // console.log('asntes => ', saida)
    
    try {
        const exists = await db.query(exists_table)
        if(exists.rows[0].exists){
            await db.query(delete_query);
            console.log('Tabela "lang_problems_ref" deletada com suceso!')
        }
        else {
            console.log("Tabela lang_problems_ref não existe")
        }
    } catch (error) {
        console.error("Erro ao deletar tabela lang_problems_ref: ", error)
    }
    return
}
