import { Request, Response } from 'express'
import db from '../database/conection'

const table = 'runtable'

export class RunController {
    async index(req: Request, res: Response) {
        const id = req.params.id_p
        db.query(`SELECT * FROM ${table} WHERE runproblem= ${id}`, (error, results) => {
            if (error) {
                console.error('retorno o erro => ', error)
                return res.status(500).send(error)
            }
            res.status(200).json(results.rows)
        })
    }

    async show(req: Request, res: Response) {

        const id = req.params.id_r
        db.query(`SELECT * FROM ${table} WHERE runnumber = ${id}`, (error, results) => {
            if (error) {
                console.error('retorno o erro => ', error)
                return res.status(500).send(error)
            }
            res.status(200).json(results.rows)
        })
    }

    async create(req: Request, res: Response) {
        const id = req.params.id_p;
        const { contestnumber,
            runsitenumber,
            runnumber,
            usernumber,
            rundate,
            rundatediff,
            rundatediffans,
            //runproblem,
            runfilename,
            rundata,
            runanswer,
            runstatus,
            runjudge,
            runjudgesite,
            runanswer1,
            runjudge1,
            runjudgesite1,
            runanswer2,
            runjudge2,
            runjudgesite2,
            runlangnumber,
            autoip,
            autobegindate,
            autoenddate,
            autoanswer,
            autostdout,
            autostderr,
            updatetime } = req.body

            const queryresponse = await db.query(`SELECT MAX(runnumber) FROM ${table} `)
            let run_num = queryresponse.rows[0].max
            if (run_num == null) {
                run_num = 0
            }
            run_num++;

        db.query(`INSERT into ${table} (contestnumber,
        runsitenumber,
        runnumber,
        usernumber,
        rundate,
        rundatediff,
        rundatediffans,
        runproblem,
        runfilename,
        rundata,
        runanswer,
        runstatus,
        runjudge,
        runjudgesite,
        runanswer1,
        runjudge1,
        runjudgesite1,
        runanswer2,
        runjudge2,
        runjudgesite2,
        runlangnumber,
        autoip,
        autobegindate,
        autoenddate,
        autoanswer,
        autostdout,
        autostderr,
        updatetime) VALUES ($1,$2,${run_num},$3,$4,$5,$6,${id},$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22,$23
            ,$24,$25,$26)`,
            [contestnumber,
                runsitenumber,
                //runnumber,
                usernumber,
                rundate,
                rundatediff,
                rundatediffans,
                // runproblem,
                runfilename,
                rundata,
                runanswer,
                runstatus,
                runjudge,
                runjudgesite,
                runanswer1,
                runjudge1,
                runjudgesite1,
                runanswer2,
                runjudge2,
                runjudgesite2,
                runlangnumber,
                autoip,
                autobegindate,
                autoenddate,
                autoanswer,
                autostdout,
                autostderr,
                updatetime],
            (error, results) => {
                if (error) {
                    throw error
                }

                res.status(200).json(results.rows);
            }
        );
    }
    async update(req: Request, res: Response) {
        const id = req.params.id_r;
        const { contestnumber,
            runsitenumber,
            runnumber,
            usernumber,
            rundate,
            rundatediff,
            rundatediffans,
            runproblem,
            runfilename,
            rundata,
            runanswer,
            runstatus,
            runjudge,
            runjudgesite,
            runanswer1,
            runjudge1,
            runjudgesite1,
            runanswer2,
            runjudge2,
            runjudgesite2,
            runlangnumber,
            autoip,
            autobegindate,
            autoenddate,
            autoanswer,
            autostdout,
            autostderr,
            updatetime } = req.body
        db.query(
            `UPDATE ${table} SET contestnumber=$1,
            runsitenumber=$2,
            usernumber=$3,
            rundate=$4,
            rundatediff=$5,
            rundatediffans=$6,
            runproblem=$7,
            runfilename=$8,
            rundata=$9,
            runanswer=$10,
            runstatus=$11,
            runjudge=$12,
            runjudgesite=$13,
            runanswer1=$14,
            runjudge1=$15,
            runjudgesite1=$16,
            runanswer2=$17,
            runjudge2=$18,
            runjudgesite2=$19,
            runlangnumber=$20,
            autoip=$21,
            autobegindate=$22,
            autoenddate=$23,
            autoanswer=$24,
            autostdout=$25,
            autostderr=$26,
            updatetime=$27
            WHERE runnumber  = ${id}`,
            [contestnumber,
                runsitenumber,
                // runnumber,
                usernumber,
                rundate,
                rundatediff,
                rundatediffans,
                runproblem,
                runfilename,
                rundata,
                runanswer,
                runstatus,
                runjudge,
                runjudgesite,
                runanswer1,
                runjudge1,
                runjudgesite1,
                runanswer2,
                runjudge2,
                runjudgesite2,
                runlangnumber,
                autoip,
                autobegindate,
                autoenddate,
                autoanswer,
                autostdout,
                autostderr,
                updatetime],
            (error, results) => {
                if (error) {
                    throw error
                }
                res.status(200).json(results.rows);
            }
        );
    }
    async delete(req: Request, res: Response){
        const id = req.params.id_r
        db.query(`DELETE FROM ${table} WHERE runnumber = ${id}`, (error, results) => {
            if (error) {
                console.error('retorno o erro => ', error)
                return res.status(500).send(error)
            }
            res.status(200).json(results.rows)
          })
    }

}