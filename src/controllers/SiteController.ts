import { Request, Response } from 'express'
import db from '../database/conection'

const table = 'sitetable'
export class SiteController {
  async index(req: Request, res: Response) {
    const id = req.params.id_c
    db.query(`SELECT * FROM ${table} WHERE contestnumber= ${id}`, (error, results) => {
      if (error) {
        console.error('retorno o erro => ', error)
        return res.status(500).send(error)
      }
      res.status(200).json(results.rows)
    })
  }

  async show(req: Request, res: Response) {

    const id = req.params.id_s
    db.query(`SELECT * FROM ${table} WHERE sitenumber = ${id}`, (error, results) => {
      if (error) {
        console.error('retorno o erro => ', error)
        return res.status(500).send(error)
      }
      res.status(200).json(results.rows)
    })
  }


  async create(req: Request, res: Response) {
    const id = req.params.id_c;
    const {
      //contestnumber,
      //sitenumber,
      siteip,
      sitename,
      siteactive,
      sitepermitlogins,
      sitelastmileanswer,
      sitelastmilescore,
      siteduration,
      siteautoend,
      sitejudging,
      sitetasking,
      siteglobalscore,
      sitescorelevel,
      sitenextuser,
      sitenextclar,
      sitenextrun,
      sitenexttask,
      sitemaxtask,
      updatetime,
      sitechiefname,
      siteautojudge,
      sitemaxruntime,
      sitemaxjudgewaittime } = req.body

    const queryresponse = await db.query(`SELECT MAX(sitenumber) FROM sitetable`)
    let site_num = queryresponse.rows[0].max
    if (site_num == null) {
      site_num = 0
    }
    site_num++;

    db.query(
      `INSERT into sitetable (contestnumber,sitenumber, siteip, sitename , siteactive,sitepermitlogins, sitelastmileanswer,
                  sitelastmilescore,
                  siteduration ,
                  siteautoend,
                  sitejudging ,
                  sitetasking  ,
                  siteglobalscore ,
                  sitescorelevel,
                  sitenextuser, 
                  sitenextclar, 
                  sitenextrun,
                  sitenexttask, 
                  sitemaxtask ,
                  updatetime ,
                  sitechiefname,
                  siteautojudge ,
                  sitemaxruntime ,
                  sitemaxjudgewaittime) 
                  VALUES (${id},${site_num},$1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22)`,
      [//contestnumber,
        // sitenumber,
        siteip,
        sitename,
        siteactive,
        sitepermitlogins,
        sitelastmileanswer,
        sitelastmilescore,
        siteduration,
        siteautoend,
        sitejudging,
        sitetasking,
        siteglobalscore,
        sitescorelevel,
        sitenextuser,
        sitenextclar,
        sitenextrun,
        sitenexttask,
        sitemaxtask,
        updatetime,
        sitechiefname,
        siteautojudge,
        sitemaxruntime,
        sitemaxjudgewaittime],
      (error, results) => {
        if (error) {
          throw error
        }
        res.status(200).json(results.rows);
      }
    );
  }

  async update(req: Request, res: Response) {
    const id = req.params.id_s;
    const {
      contestnumber,
      sitenumber,
      siteip,
      sitename,
      siteactive,
      sitepermitlogins,
      sitelastmileanswer,
      sitelastmilescore,
      siteduration,
      siteautoend,
      sitejudging,
      sitetasking,
      siteglobalscore,
      sitescorelevel,
      sitenextuser,
      sitenextclar,
      sitenextrun,
      sitenexttask,
      sitemaxtask,
      updatetime,
      sitechiefname,
      siteautojudge,
      sitemaxruntime,
      sitemaxjudgewaittime } = req.body

    db.query(
      `UPDATE sitetable SET siteip = $1, sitename = $2, siteactive= $3,sitepermitlogins = $4, sitelastmileanswer =$5,
		sitelastmilescore = $6,
		siteduration = $7,
		siteautoend = $8,
		sitejudging = $9,
		sitetasking = $10 ,
		siteglobalscore = $11,
		sitescorelevel = $12,
		sitenextuser = $13,
		sitenextclar = $14,
		sitenextrun = $15,
		sitenexttask = $16,
		sitemaxtask = $17,
		updatetime = $18,
		sitechiefname = $19,
		siteautojudge = $20,
		sitemaxruntime = $21,
		sitemaxjudgewaittime = $22
       WHERE sitenumber = ${id} AND contestnumber = $23`,
      [
        siteip,
        sitename,
        siteactive,
        sitepermitlogins,
        sitelastmileanswer,
        sitelastmilescore,
        siteduration,
        siteautoend,
        sitejudging,
        sitetasking,
        siteglobalscore,
        sitescorelevel,
        sitenextuser,
        sitenextclar,
        sitenextrun,
        sitenexttask,
        sitemaxtask,
        updatetime,
        sitechiefname,
        siteautojudge,
        sitemaxruntime,
        sitemaxjudgewaittime,
        contestnumber],
      (error, results) => {
        if (error) {
          throw error
        }
        res.status(200).json(results.rows);
      }
    );
  }

  async delete(req: Request, res: Response) {
    const id = req.params.id_s
    db.query(`DELETE FROM ${table} WHERE sitenumber = ${id}`, (error, results) => {
      if (error) {
        console.error('retorno o erro => ', error)
        return res.status(500).send(error)
      }
      res.status(200).json(results.rows)
    })
  }
}