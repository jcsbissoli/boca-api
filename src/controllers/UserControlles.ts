import { Request, Response } from "express";
import db from "../database/conection";

const table = 'usertable'
export class UserController {
  async index(req: Request, res: Response) {
    const id = req.params.id_c
    db.query(`SELECT * FROM usertable WHERE contestnumber= ${id}`, (error, results) => {
      if (error) {
        console.error("retorno o erro => ", error);
        return res.status(500).send(error);
      }
      res.status(200).json(results.rows);
    });
  }


  async show(req: Request, res: Response) {
    const id = req.params.id_u
    db.query(`SELECT * FROM ${table} WHERE usernumber = ${id}`, (error, results) => {
      if (error) {
        console.error('retorno o erro => ', error)
        return res.status(500).send(error)
      }
      res.status(200).json(results.rows)
    })
  }

  async create(req: Request, res: Response) {
    const id = req.params.id_c;
    const {
      contestnumber,
      usersitenumber,
      usernumber,
      username,
      userfullname,
      userdesc,
      usertype,
      userenabled,
      usermultilogin,
      userpassword,
      userip,
      userlastlogin,
      usersession,
      usersessionextra,
      userlastlogout,
      userpermitip,
      userinfo,
      usericpcid } = req.body;
    
      const queryresponse = await db.query(`SELECT MAX(usernumber) FROM ${table} `)
      let user_num = queryresponse.rows[0].max
      if (user_num == null) {
          user_num = 0
      }
      user_num++;
    
    db.query(
      `INSERT into usertable (contestnumber,
        usersitenumber,
        usernumber,
        username,
        userfullname,
        userdesc,
        usertype,
        userenabled,
        usermultilogin,
        userpassword,
        userip,
        userlastlogin,
        usersession,
        usersessionextra,
        userlastlogout,
        userpermitip,
        userinfo,
        usericpcid) 
        VALUES (${id},$1,${user_num},$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16)`,
      [//contestnumber,
        usersitenumber,
        // usernumber,
        username,
        userfullname,
        userdesc,
        usertype,
        userenabled,
        usermultilogin,
        userpassword,
        userip,
        userlastlogin,
        usersession,
        usersessionextra,
        userlastlogout,
        userpermitip,
        userinfo,
        usericpcid],
      (error, results) => {
        if (error) {
          throw error
        }
        res.status(200).json(results.rows);
      }
    );
  }

  async update(req: Request, res: Response) {
    const id = req.params.id_u;
    const {
      contestnumber,
      usersitenumber,
      usernumber,
      username,
      userfullname,
      userdesc,
      usertype,
      userenabled,
      usermultilogin,
      userpassword,
      userip,
      userlastlogin,
      usersession,
      usersessionextra,
      userlastlogout,
      userpermitip,
      userinfo,
      usericpcid } = req.body;
    db.query(
      `UPDATE usertable SET contestnumber = $1,
      usersitenumber= $2,
      usernumber= $3,
      username= $4,
      userfullname= $5,
      userdesc= $6,
      usertype= $7,
      userenabled= $8,
      usermultilogin= $9,
      userpassword= $10,
      userip= $11,
      userlastlogin= $12,
      usersession= $13,
      usersessionextra= $14,
      userlastlogout= $15,
      userpermitip= $16,
      userinfo= $17,
      usericpcid= $18
		  WHERE usernumber = ${id}`,
      [contestnumber,
        usersitenumber,
        usernumber,
        username,
        userfullname,
        userdesc,
        usertype,
        userenabled,
        usermultilogin,
        userpassword,
        userip,
        userlastlogin,
        usersession,
        usersessionextra,
        userlastlogout,
        userpermitip,
        userinfo,
        usericpcid],
      (error, results) => {
        if (error) {
          throw error
        }
        res.status(200).json(results.rows);
      }
    );
  }

  async delete(req: Request, res: Response) {
    const id = req.params.id_u
    db.query(`DELETE FROM usertable WHERE usernumber = ${id}`, (error, results) => {
      if (error) {
        console.error('retorno o erro => ', error)
        return res.status(500).send(error)
      }
      res.status(200).json(results.rows)
    })
  }
}
