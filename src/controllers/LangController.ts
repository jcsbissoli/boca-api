import { Request, Response } from 'express'
import db from '../database/conection'

const table = 'langtable'
export class LangController {
    async index(req: Request, res: Response) {
        const id = req.params.id_c
        db.query(`SELECT * FROM ${table} WHERE contestnumber= ${id}`, (error, results) => {
            if (error) {
                console.error('retorno o erro => ', error)
                return res.status(500).send(error)
            }
            res.status(200).json(results.rows)
        })
    }
    async show(req: Request, res: Response) {

        const id = req.params.id_l
        db.query(`SELECT * FROM ${table} WHERE langnumber = ${id}`, (error, results) => {
            if (error) {
                console.error('retorno o erro => ', error)
                return res.status(500).send(error)
            }
            res.status(200).json(results.rows)
        })
    }
    async create(req: Request, res: Response) {
        const id = req.params.id_c;
        const { contestnumber,
            langnumber,
            langname,
            langextension,
            updatetime } = req.body
            
        const queryresponse = await db.query(`SELECT MAX(langnumber) FROM ${table} `)
        let lang_num = queryresponse.rows[0].max
        if (lang_num == null) {
            lang_num = 0
        }
        lang_num++;

        db.query(`INSERT into langtable(contestnumber,
            langnumber,
            langname,
            langextension,
            updatetime)
            VALUES (${id},${lang_num},$1,$2,$3)`,
            [//langnumber,
                langname,
                langextension,
                updatetime]
            ,
            (error, results) => {
                if (error) {
                    throw error
                }

                res.status(200).json(results.rows);
            }
        );
    }
    async update(req: Request, res: Response) {
        const id = req.params.id_l;
        const { contestnumber,
            langnumber,
            langname,
            langextension,
            updatetime } = req.body
        db.query(
            `UPDATE langtable SET
                contestnumber =$1,
            langname=$2,
            langextension=$3,
            updatetime=$4
            WHERE langnumber=${id}`,
            [contestnumber,
                langname,
                langextension,
                updatetime]
            ,
            (error, results) => {
                if (error) {
                    throw error
                }
                res.status(200).json(results.rows);
            }
        );
    }
    async delete(req: Request, res: Response) {
        const id = req.params.id_l
        db.query(`DELETE FROM ${table} WHERE langnumber = ${id}`, (error, results) => {
            if (error) {
                console.error('retorno o erro => ', error)
                return res.status(500).send(error)
            }
            res.status(200).json(results.rows)
        })
    }

}    