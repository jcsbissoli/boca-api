import { Request, Response } from "express";
import db from "../database/conection";

const table = "contesttable";

export class ContestController {
  async index(req: Request, res: Response) {
    
    db.query("SELECT * FROM contesttable", (error, results) => {
      if (error) {
        console.error("retorno o erro => ", error);
        return res.status(500).send(error);
      }
      res.status(200).json(results.rows);
    });
  }

  async show(req: Request, res: Response) {
    console.log("req => ", req.params.id_c);
    const id = req.params.id_c;
    db.query(
      `SELECT * FROM contesttable WHERE contestnumber = ${id}`,
      (error, results) => {
        if (error) {
          console.error("retorno o erro => ", error);
          return res.status(500).send(error);
        }
        res.status(200).json(results.rows);
      }
    );
  }

  async create(req: Request, res: Response) {

    const { 
      contestname,
      conteststartdate,
      contestduration,
      contestlastmileanswer,
      contestlastmilescore,
      contestlocalsite,
      contestpenalty,
      contestmaxfilesize,
      contestactive,
      contestmainsite,
      contestkeys,
      contestunlockkey,
      contestmainsiteurl
       } = req.body

    const queryresponse = await db.query(`SELECT MAX(contestnumber) FROM contesttable`)
    let contest_id = queryresponse.rows[0].max
    if (contest_id == null) {
      contest_id = 0
    }
    contest_id++;
    
    db.query(
      `INSERT INTO contesttable (contestnumber,contestname,conteststartdate,contestduration,contestlastmileanswer,contestlastmilescore,contestlocalsite,
          contestpenalty,contestmaxfilesize,contestactive,contestmainsite,contestkeys,contestunlockkey,contestmainsiteurl
          ) VALUES (${contest_id},$1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13)`, [contestname, conteststartdate, contestduration, contestlastmileanswer, contestlastmilescore, contestlocalsite,
      contestpenalty, contestmaxfilesize, contestactive, contestmainsite, contestkeys, contestunlockkey, contestmainsiteurl
      ], (error, results) => {
        if (error) {
          console.error("retorno o erro => ", error);
          return res.status(500).send(error);
        }
        res.status(200).json(results.rows);
      }
    );

  }

  async update(req: Request, res: Response) {
    const id = req.params.id_c;
    const {
      contestname,
      conteststartdate,
      contestduration,
      contestlastmileanswer,
      contestlastmilescore,
      contestlocalsite,
      contestpenalty,
      contestmaxfilesize,
      contestactive,
      contestmainsite,
      contestkeys,
      contestunlockkey,
      contestmainsiteurl,
      } = req.body
    db.query(
      `UPDATE contesttable SET contestname = $1, conteststartdate = $2, contestduration = $3,contestlastmileanswer = $4,contestlastmilescore = $5,contestlocalsite = $6,contestpenalty = $7,contestmaxfilesize = $8,contestactive = $9,contestmainsite = $10,contestkeys = $11,contestunlockkey = $12,contestmainsiteurl = $13 WHERE contestnumber = ${id}`,
      [contestname, conteststartdate, contestduration, contestlastmileanswer, contestlastmilescore, contestlocalsite,
        contestpenalty, contestmaxfilesize, contestactive, contestmainsite, contestkeys, contestunlockkey, contestmainsiteurl
        ],
      (error, results) => {
        if (error) {
          throw error
        }
        res.status(200).json(results.rows);
      }
    );
  }

  async delete(req: Request, res: Response) {
    console.log("req => ", req.params.id_c);
    const id = req.params.id_c;
    db.query(
      `DELETE FROM contesttable WHERE contestnumber = ${id}`,
      (error, results) => {
        if (error) {
          console.error("retorno o erro => ", error);
          return res.status(500).send(error);
        }
        res.status(200).json(results.rows);
      }
    );
  }

}
