import { Request, Response } from 'express'
import db from '../database/conection'
import { WorkingController } from './WorkingController'

const table = 'problemtable'
export class ProblemController {
    async index(req: Request, res: Response) {
        const id = req.params.id_c
        db.query(`SELECT * FROM ${table} WHERE contestnumber= ${id}`, (error, results) => {
            if (error) {
                console.error('retorno o erro => ', error)
                return res.status(500).send(error)
            }
            res.status(200).json(results.rows)
        })
    }
    async show(req: Request, res: Response) {

        const id = req.params.id_p
        db.query(`SELECT * FROM ${table} WHERE problemnumber = ${id}`, (error, results) => {
            if (error) {
                console.error('retorno o erro => ', error)
                return res.status(500).send(error)
            }
            res.status(200).json(results.rows)
        })
    }




    async create(req: Request, res: Response) {
        const id = req.params.id_c;
        const { contestnumber,
            problemnumber,
            problemname,
            problemfullname,
            problembasefilename,
            probleminputfilename,
            probleminputfile,
            probleminputfilehash,
            fake,
            problemcolorname,
            problemcolor,
            updatetime, working_id} = req.body

        const queryresponse = await db.query(`SELECT MAX(problemnumber) FROM ${table} `)
        let problem_num = queryresponse.rows[0].max
        if (problem_num == null) {
            problem_num = 0
        }
        problem_num++;

        db.query(`INSERT into problemtable(contestnumber,
            problemnumber,
            problemname,
            problemfullname,
            problembasefilename,
            probleminputfilename,
            probleminputfile,
            probleminputfilehash,
            fake,
            problemcolorname,
            problemcolor,
            updatetime,working_id)
            VALUES (${id},${problem_num},$1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11)`,
            [//contestnumber,
                // problemnumber,
                problemname,
                problemfullname,
                problembasefilename,
                probleminputfilename,
                probleminputfile,
                probleminputfilehash,
                fake,
                problemcolorname,
                problemcolor,
                updatetime,
                working_id],
            (error, results) => {
                if (error) {
                    throw error
                }

                res.status(200).json(results.rows);
            }
        );
    }
    async update(req: Request, res: Response) {
        const id = req.params.id_p;
        const { contestnumber,
            problemnumber,
            problemname,
            problemfullname,
            problembasefilename,
            probleminputfilename,
            probleminputfile,
            probleminputfilehash,
            fake,
            problemcolorname,
            problemcolor,
            updatetime } = req.body
        db.query(
            `UPDATE problemtable SET contestnumber=$1,
            problemname=$2,
            problemfullname=$3,
            problembasefilename=$4,
            probleminputfilename=$5,
            probleminputfile=$6,
            probleminputfilehash=$7,
            fake=$8,
            problemcolorname=$9,
            problemcolor=$10,
            updatetime=$11
            WHERE problemnumber = ${id}`,
            [contestnumber,
                //problemnumber,
                problemname,
                problemfullname,
                problembasefilename,
                probleminputfilename,
                probleminputfile,
                probleminputfilehash,
                fake,
                problemcolorname,
                problemcolor,
                updatetime]
            ,
            (error, results) => {
                if (error) {
                    throw error
                }
                res.status(200).json(results.rows);
            }
        );
    }
    async delete(req: Request, res: Response) {
        const id = req.params.id_p
        db.query(`DELETE FROM ${table} WHERE problemnumber = ${id}`, (error, results) => {
            if (error) {
                console.error('retorno o erro => ', error)
                return res.status(500).send(error)
            }
            res.status(200).json(results.rows)
        })
    }


    async add_language(req: Request, res: Response) {
        const id = req.params.id_p;
        const { langnumber_array } = req.body;
        langnumber_array.forEach(langnumber => {
            db.query(`INSERT into lang_problems_ref (problemnumber,langnumber)VALUES ($1,$2)`, [id, langnumber],
                (error, results) => {
                    if (error) {
                        throw error
                    }


                }
            );
        });
        res.status(200).json("cadastrada(s) linguagem(s)");
    }
    async index_language(req: Request, res: Response) {
        const id = req.params.id_p;

        db.query(`SELECT langname,lang_problems_ref.langnumber FROM lang_problems_ref INNER JOIN langtable 
        ON lang_problems_ref.langnumber=langtable.langnumber WHERE problemnumber = ${id}`,
            (error, results) => {
                if (error) {
                    throw error
                }

                res.status(200).json(results.rows);
            }
        );
    }
    async delete_language(req: Request, res: Response) {
        const id = req.params.id_p;
        const { langnumber_array } = req.body;
        langnumber_array.forEach(langnumber => {
            db.query(`DELETE FROM lang_problems_ref WHERE problemnumber=${id} AND langnumber=${langnumber}`,
                (error, results) => {
                    if (error) {
                        throw error
                    }


                }
            );
        });
        res.status(200).json("linguagem(s) removida(s)");
    }
}
