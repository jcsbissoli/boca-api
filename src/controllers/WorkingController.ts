import { Request, Response } from "express";
import db from "../database/conection";

const table = "workingtable";

export class WorkingController {
    async create(req: Request, res: Response) {
        const id = req.params.id_c;
        const {
            name,
            contestnumber,
            endDate,
            maxfileSize,
            updadtedAt,
            createdAt,
            deletedAt,
            isMultilogin,
            isDeleted} = req.body
            const queryresponse = await db.query(`SELECT MAX(id) FROM ${table} `)
            let working_id = queryresponse.rows[0].max
            if (working_id == null) {
                working_id = 0
            }
            working_id++;    
        db.query(
          `INSERT into ${table} (id,name,
            contestnumber,
            endDate,
            maxfileSize,
            updadtedAt,
            createdAt,
            deletedAt,
            isMultilogin,
            isDeleted)
            VALUES (${working_id},$1,${id},$2,$3,$4,$5,$6,$7,$8)`,
          [name,
            endDate,
            maxfileSize,
            updadtedAt,
            createdAt,
            deletedAt,
            isMultilogin,
            isDeleted],
          (error, results) => {
            if (error) {
              throw error
            }
            res.status(200).json(results.rows);
          }
        );
      }
      async delete(req: Request, res: Response) {
        const id = req.params.id_w
        
        await db.query(`DELETE FROM working_user_ref WHERE working_id=${id}`)

        db.query(`DELETE FROM ${table} WHERE id = ${id}`, (error, results) => {
          if (error) {
            console.error('retorno o erro => ', error)
            return res.status(500).send(error)
          }
          res.status(200).json(results.rows)
        })
      }


    async create_working_user_ref(req: Request, res: Response) {
        const id = req.params.id_w;
        const { usernumber_array } = req.body;
        usernumber_array.forEach(usernumber => {
            db.query(`INSERT into working_user_ref (usernumber,working_id)VALUES (${usernumber},${id})`,
                (error, results) => {
                    if (error) {
                        throw error
                    }


                }
            );
        });
        res.status(200).json("usuario adicionado a lista");
    }
    async index_working_user_ref(req: Request, res: Response) {
        const id = req.params.id_w;

        db.query(`SELECT * FROM working_user_ref INNER JOIN usertable 
      ON  working_user_ref.usernumber=usertable.usernumber WHERE working_user_ref.working_id = ${id}`,
            (error, results) => {
                if (error) {
                    throw error
                }

                res.status(200).json(results.rows);
            }
        );
    }
    async delete_working_user_ref(req: Request, res: Response) {
        const id = req.params.id_w;
        const { usernumber_array } = req.body;
        usernumber_array.forEach(usernumber => {
            db.query(`DELETE FROM working_user_ref WHERE working_id=${id}  AND usernumber=${usernumber}`,
                (error, results) => {
                    if (error) {
                        throw error
                    }


                }
            );
        });
        res.status(200).json("usuario(s) removido(s)");
    }
    async add_list_to_user(req: Request, res: Response) {
        const id = req.params.id_u;
        const { list_array } = req.body;
        list_array.forEach(working_id => {
            db.query(`INSERT into working_user_ref (usernumber,working_id)VALUES (${id},${working_id})`,
                (error, results) => {
                    if (error) {
                        throw error
                    }


                }
            );
        });
        res.status(200).json("lista adicionada a usuario");
    }

    async index_list_by_users(req: Request, res: Response) {
        const id = req.params.id_u;

        db.query(`SELECT * FROM working_user_ref INNER JOIN workingtable 
  ON  working_user_ref.working_id=workingtable.id WHERE working_user_ref.usernumber = ${id}`,
            (error, results) => {
                if (error) {
                    throw error
                }

                res.status(200).json(results.rows);
            }
        );
    }

    async delete_working_from_user(req: Request, res: Response) {
        const id = req.params.id_u;
        const { list_array } = req.body;

        list_array.forEach(working_id => {
            db.query(`DELETE FROM working_user_ref WHERE working_id=${working_id}  AND usernumber=${id}`,
                (error, results) => {
                    if (error) {
                        throw error
                    }


                }
            );
        });
        res.status(200).json("working(s) removido(s)");
    }
  
}
