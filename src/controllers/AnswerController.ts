import { Request, Response } from "express";
import db from "../database/conection";

const table = "answertable";

export class AnswerController {
    async index(req: Request, res: Response) {
        const id = req.params.id_c
        db.query(`SELECT * FROM ${table} WHERE contestnumber= ${id}`, (error, results) => {
            if (error) {
                console.error('retorno erro => ', error)
                return res.status(500).send(error)
            }
            res.status(200).json(results.rows)
        })
    }
    async show(req: Request, res: Response) {

        const id = req.params.id_a
        db.query(`SELECT * FROM ${table} WHERE answernumber = ${id}`, (error, results) => {
            if (error) {
                console.error('retorno erro => ', error)
                return res.status(500).send(error)
            }
            res.status(200).json(results.rows)
        })
    }
    async create(req: Request, res: Response) {
        const id = req.params.id_c;
        const {
            contestnumber,
            answernumber,
            runanswer,
            yes,
            fake,
            updatetime
        } = req.body

        const queryresponse = await db.query(`SELECT MAX(answernumber) FROM ${table} `)
        let ans_num = queryresponse.rows[0].max
        if (ans_num == null) {
            ans_num = 0
        }
        ans_num++;

        db.query(`INSERT into ${table} (contestnumber,
		    answernumber,
		    runanswer,
		    yes,
		    fake,
		    updatetime)
            VALUES (${id},${ans_num},$1,$2,$3,$4)`,
            [//answernumber,
                runanswer,
                yes,
                fake,
                updatetime],
            (error, results) => {
                if (error) {
                    throw error
                }

                res.status(200).json(results.rows);
            }
        );
    }
    async update(req: Request, res: Response) {
        const id = req.params.id_a;
        const {
            contestnumber,
            // answernumber,
            runanswer,
            yes,
            fake,
            updatetime
        } = req.body
        db.query(`UPDATE ${table} SET
        contestnumber=$1,
            runanswer=$2,
            yes=$3,
            fake=$4,
            updatetime=$5
            WHERE answernumber=${id}`,
            [contestnumber,
                runanswer,
                yes,
                fake,
                updatetime]
            ,
            (error, results) => {
                if (error) {
                    throw error
                }
                res.status(200).json(results.rows);
            }
        );

    }
    async delete(req: Request, res: Response) {
        const id = req.params.id_a
        db.query(`DELETE FROM ${table} WHERE answernumber = ${id}`, (error, results) => {
            if (error) {
                console.error('retorno erro => ', error)
                return res.status(500).send(error)
            }
            res.status(200).json(results.rows)
        })

    }
}