import {Router, Response, Request} from   'express'
import { UserController } from './controllers/UserControlles'
import { ContestController } from './controllers/ContestController'
import { SiteController } from './controllers/SiteController'
import { ProblemController } from './controllers/ProblemController'
import { LangController } from './controllers/LangController'
import { RunController } from './controllers/RunController'
import { AnswerController } from './controllers/AnswerController'
import { WorkingController } from './controllers/WorkingController'


const router =  Router()

router.get('/contest/:id_c/user', new UserController().index)
router.post('/contest/:id_c/user', new UserController().create)
router.get('/user/:id_u', new UserController().show)
router.put('/user/:id_u', new UserController().update)
router.delete('/user/:id_u', new UserController().delete)


router.get('/contest', new ContestController().index)
router.post('/contest/', new ContestController().create)
router.get('/contest/:id_c', new ContestController().show)
router.put('/contest/:id_c', new ContestController().update)
router.delete('/contest/:id_c', new ContestController().delete)


router.get('/contest/:id_c/site', new SiteController().index)
router.get('/site/:id_s', new SiteController().show)
router.post('/contest/:id_c/site', new SiteController().create)
router.put('/site/:id_s', new SiteController().update)
router.delete('/site/:id_s', new SiteController().delete)


router.get('/contest/:id_c/language', new LangController().index)
router.get('/language/:id_l', new LangController().show)
router.post('/contest/:id_c/language', new LangController().create)
router.put('/language/:id_l', new LangController().update)
router.delete('/language/:id_l', new LangController().delete)

router.get('/contest/:id_c/answertable', new AnswerController().index)
router.get('/answertable/:id_a', new AnswerController().show)
router.post('/contest/:id_c/answertable', new AnswerController().create)
router.put('/answertable/:id_a', new AnswerController().update)
router.delete('/answertable/:id_a', new AnswerController().delete)

router.get('/problem/:id_p/run', new RunController().index)
router.get('/run/:id_r', new RunController().show)
router.post('/problem/:id_p/run', new RunController().create)
router.put('/run/:id_r', new RunController().update)
router.delete('/run/:id_r', new RunController().delete)

router.delete('/problem/:id_p', new ProblemController().delete)
router.put('/problem/:id_p', new ProblemController().update)
router.get('/problem/:id_p', new ProblemController().show)
router.get('/contest/:id_c/problem', new ProblemController().index)
router.post('/contest/:id_c/problem', new ProblemController().create)



router.post('/problem/:id_p/language', new ProblemController().add_language)
router.get('/problem/:id_p/language', new ProblemController().index_language)
router.delete('/problem/:id_p/language', new ProblemController().delete_language)


router.post('/contest/:id_c/workingtable', new WorkingController().create)
router.delete('/working/:id_w', new WorkingController().delete)


router.get('/working/:id_w/user', new WorkingController().index_working_user_ref)
router.post('/working/:id_w/user', new WorkingController().create_working_user_ref)
router.delete('/working/:id_w/user', new WorkingController().delete_working_user_ref)
router.post('/user/:id_u/working', new WorkingController().add_list_to_user)
router.get('/user/:id_u/working', new WorkingController().index_list_by_users)
router.delete('/user/:id_u/working', new WorkingController().delete_working_from_user)

export default router